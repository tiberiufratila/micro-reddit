class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.text :body
      t.references :user, foreign_key: {to_table: :users}
      t.references :post, foreign_key: {to_table: :posts}
      
      t.timestamps
    end
  end
end
